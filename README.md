# Facets Custom Label

Facets Custom Label module is a facet processor which lets you rename / relabel
facets items.

* For a full description of the module, visit the project page:
  <https://www.drupal.org/project/facets_custom_label>

* To submit bug reports and feature suggestions, or track changes:
  <https://www.drupal.org/project/issues/facets_custom_label>

## Requirements

_Facets Custom Label_ works on Drupal 8 and beyond.

The following modules are required:

* Facets (<https://www.drupal.org/project/facets>)
* Configuration Translation (from core), only if you need translated labels.

## Installation

* Install as you would normally install a contributed Drupal module. Visit
  <https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules>
  for further information.

## Configuration

* Go to the facet onto which the label processor has to be enabled.
* Enable the **Facets custom label processor**.
* In the configuration form, insert the value mapping in the textarea box.
  Syntax can be found here:
  <https://www.drupal.org/project/facets_custom_label>
* (optional) If you need translated labels, enable the
  **Configuration Translation** module and translate the facet configuration.
